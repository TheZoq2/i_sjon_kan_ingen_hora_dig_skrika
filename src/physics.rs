extern crate nalgebra as na;

use na::Vector3;
use nphysics3d::object::{DefaultBodySet, DefaultColliderSet};
use nphysics3d::force_generator::DefaultForceGeneratorSet;
use nphysics3d::joint::DefaultJointConstraintSet;
use nphysics3d::world::{DefaultMechanicalWorld, DefaultGeometricalWorld};
use nphysics3d::object::{Body, DefaultBodyHandle};

pub struct PhysicsWorld {
    pub mechanical_world: DefaultMechanicalWorld<f32>,
    geometrical_world: DefaultGeometricalWorld<f32>,

    bodies: DefaultBodySet<f32>,
    colliders: DefaultColliderSet<f32>,
    joint_constraints: DefaultJointConstraintSet<f32>,
    force_generators: DefaultForceGeneratorSet<f32>,
}

impl PhysicsWorld {
    pub fn new() -> Self {
        let mut mechanical_world = DefaultMechanicalWorld::new(Vector3::new(0.0, -9.81, 0.0));
        let geometrical_world = DefaultGeometricalWorld::new();

        let bodies = DefaultBodySet::new();
        let colliders = DefaultColliderSet::new();
        let joint_constraints = DefaultJointConstraintSet::new();
        let force_generators = DefaultForceGeneratorSet::new();

        mechanical_world.set_timestep(1./60.);

        Self {
            mechanical_world,
            geometrical_world,
            bodies,
            colliders,
            joint_constraints,
            force_generators
        }
    }

    pub fn step(&mut self) {
        self.mechanical_world.step(
            &mut self.geometrical_world,
            &mut self.bodies,
            &mut self.colliders,
            &mut self.joint_constraints,
            &mut self.force_generators
        )
    }

    pub fn add_body(&mut self, body: impl Body<f32>) -> DefaultBodyHandle {
        self.bodies.insert(body)
    }

    #[inline(always)]
    pub fn get_body(&self, handle: DefaultBodyHandle) -> Option<&dyn Body<f32>> {
        self.bodies.get(handle)
    }
    #[inline(always)]
    pub fn get_body_mut(&mut self, handle: DefaultBodyHandle) -> Option<&mut dyn Body<f32>> {
        self.bodies.get_mut(handle)
    }
}
