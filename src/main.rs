use bevy::prelude::*;
use bevy::input::{keyboard::KeyCode, Input};
use bevy::render::{
    mesh::shape,
    pipeline::{
        DynamicBinding,
        PipelineDescriptor,
        PipelineSpecialization,
        RenderPipeline,
        BindingDescriptor,
        BindGroupDescriptor,
        BindGroupDescriptorId,
        BindType,
        UniformProperty,
        BindingShaderStage,
    },
    render_graph::{base, AssetRenderResourcesNode, RenderGraph},
    renderer::RenderResources,
    shader::{ShaderStage, ShaderStages},
};

use nalgebra as na;
use nphysics3d::object::{RigidBodyDesc, RigidBody, Body, DefaultBodyHandle};
use nphysics3d::algebra::ForceType;

mod physics;
mod boat_data;
mod water;

use physics::PhysicsWorld;

fn main() {
    App::build()
        // .add_resource(Msaa { samples: 4 })
        .add_default_plugins()
        .add_resource(PhysicsWorld::new())
        .add_resource(PhysicsTimer(0.))
        .add_asset::<WaterMaterial>()
        .add_startup_system(setup.system())
        .add_system(water_update.system())
        .add_system(camera_control_system.system())
        .add_system(rowing_control_system.system())
        .add_system(boat_physics_graphics_updater.system())
        .add_system(physics_update.system())
        .add_system(float_manager.system())
        .add_system(wave_probe_system.system())
        .run();
}

#[derive(RenderResources, Default)]
struct WaterMaterial {
    pub color: Color,
    // Seconds since startup
    pub time: f32,

    pub camera: Vec3,
}

fn water_update(
    time: Res<Time>,
    mut water_mats: ResMut<Assets<WaterMaterial>>,
    mut water_query: Query<&Handle<WaterMaterial>>,
    mut camera_query: Query<(&Translation, &Camera)>,
) {
    for water in &mut water_query.iter() {
        if let Some(water) = water_mats.get_mut(water) {
            water.time = time.seconds_since_startup as f32;
            // water.camera = translation.0
            for (translation, _) in &mut camera_query.iter() {
                water.camera = translation.0;
            }
        }
    }
}

const VERTEX_SHADER: &str = include_str!("../media/shaders/water.vert");
const FRAGMENT_SHADER: &str = include_str!("../media/shaders/water.frag");

// Time since the last physics update in seconds
struct PhysicsTimer(f32);
struct Camera;
struct Boat(DefaultBodyHandle);
// A depth probe with the specified surface area
struct DepthProbe(f32);
// Object used to probe wave height
struct WaveProbe;

fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut pipelines: ResMut<Assets<PipelineDescriptor>>,
    mut shaders: ResMut<Assets<Shader>>,
    mut shader_materials: ResMut<Assets<WaterMaterial>>,
    mut render_graph: ResMut<RenderGraph>,
    mut physics: ResMut<PhysicsWorld>
) {
    let mut water_descriptor = PipelineDescriptor::default_config(
        ShaderStages {
            vertex: shaders.add(
                Shader::from_glsl(ShaderStage::Vertex, VERTEX_SHADER)
            ),
            fragment: Some(
                shaders.add(Shader::from_glsl(ShaderStage::Fragment, FRAGMENT_SHADER))
            )
        }
    );

    water_descriptor.layout.as_mut().map(|layout| {
        layout.bind_groups.push(
            BindGroupDescriptor::new(2, vec![
                BindingDescriptor {
                    name: "WaterProperties".to_string(),
                    index: 1,
                    bind_type: BindType::Uniform {
                        dynamic: false,
                        properties: vec![UniformProperty::Struct (vec![
                            UniformProperty::Vec4,
                            UniformProperty::Float,
                            UniformProperty::Vec3,
                        ])]
                    },
                    shader_stage: BindingShaderStage::VERTEX | BindingShaderStage::FRAGMENT
                }
            ]),
        )
    });


    // Create a new shader pipeline
    let pipeline_handle = pipelines.add(
        water_descriptor
    );

    // Add an AssetRenderResourcesNode to our Render Graph. This will bind WaterMaterial resources to our shader
    render_graph.add_system_node(
        "water_material",
        AssetRenderResourcesNode::<WaterMaterial>::new(true),
    );

     // Add a Render Graph edge connecting our new "my_material" node to the main pass node. This
     // ensures "my_material" runs before the main pass 
    render_graph
        .add_node_edge("water_material", base::node::MAIN_PASS)
        .unwrap();

    // Create a new material
    let water_material = shader_materials.add(WaterMaterial {
        color: Color::rgb(0.00, 0.2, 0.5),
        // color: Color::rgb(1.0, 1.0, 1.0),
        time: 0.,
        camera: Vec3::new(0., 0., 0.),
    });


    for x in -5..5 {
        for y in -5..5 {
            let scale = 15.;
            let water_mesh = MeshComponents {
                mesh: asset_server.load("media/water.gltf").expect("failed to load water plane"),
                render_pipelines: RenderPipelines::from_pipelines(vec![RenderPipeline::specialized(
                    pipeline_handle,
                    // NOTE: in the future you wont need to manually declare dynamic bindings
                    PipelineSpecialization {
                        dynamic_bindings: vec![
                            // Transform
                            DynamicBinding {
                                bind_group: 1,
                                binding: 0,
                            },
                            // // MyMaterial_color
                            // DynamicBinding {
                            //     bind_group: 2,
                            //     binding: 0,
                            // },
                            // // MyMaterial_color
                            // DynamicBinding {
                            //     bind_group: 1,
                            //     binding: 2,
                            // },
                            // // MyMaterial_camera
                            // DynamicBinding {
                            //     bind_group: 1,
                            //     binding: 3,
                            // },
                        ],
                        ..Default::default()
                    },
                )]),
                translation: Translation::new(x as f32 * scale, 0.0, y as f32 * scale),
                scale: Scale(scale),
                ..Default::default()
            };

            commands.spawn(water_mesh).with(water_material);
        }
    }

    let boat_mass = 120.;
    let boat_body = physics.add_body(
        RigidBodyDesc::new()
            .mass(boat_mass)
            .angular_inertia(na::Matrix3::new(
                0.716,    -0.00092, -0.00017,
                -0.00092, 5.3997,   0.00116,
                -0.00017, 0.00116,  5.7869
            ) * boat_mass)
            // .angular_damping()
            .linear_damping(0.8)
            // .gravity_enabled(false)
            .build()
    );

    // let cube = meshes.add(Mesh::from(shape::Cube { size: 1.0 }));
    // meshes.add(Mesh::from(shape::Plane { size: 10000.0 }));
    // add entities to the world
    let wave_marker = meshes.add(Mesh::from(shape::Cube { size: 0.05 }));
    commands
        // mesh
        .spawn(PbrComponents {
            // load the mesh
            mesh: asset_server
                .load("media/boat.gltf")
                .expect("failed to load boat"),
            // create a material for the mesh
            material: materials.add(Color::rgb(0.5, 0.4, 0.3).into()),
            transform: Transform::new_sync_disabled(Mat4::identity()),
            ..Default::default()
        })
        .with_children(|parent| {
            let boat_center_of_mass = Vec3::new(-0.36216, 0., -0.00106);
            let probe_marker = meshes.add(Mesh::from(shape::Cube { size: 0.05 }));
            for (pos, percentage) in boat_data::depth_probes() {
                parent
                    // .spawn(PbrComponents {
                    //     mesh: probe_marker,
                    //     material: materials.add(Color::rgb(1.0, 1.0, 1.0).into()),
                    //     translation: (pos - boat_center_of_mass).into(),
                    //     .. Default::default()
                    // })
                    .spawn((Translation::from(pos - boat_center_of_mass),))
                    .with(Transform::new(Mat4::identity()))
                    .with(DepthProbe(0.2 * 0.75 * percentage));
            }
        })
        .with(Boat(boat_body))

        .spawn(LightComponents {
            translation: Translation::new(4.0, 5.0, 4.0),
            ..Default::default()
        })
        // .spawn(PbrComponents {
        //     // load the mesh
        //     transform: Transform::new(Mat4::identity()),
        //     mesh: meshes.add(Mesh::from(shape::Icosphere {radius: 0.1, subdivisions: 10})),
        //     material: materials.add(Color::rgb(1.0, 1.0, 1.0).into()),
        //     ..Default::default()
        // })
        // .with(WaveProbe)
        // camera
        .spawn(Camera3dComponents {
            transform: Transform::new(Mat4::face_toward(
                Vec3::new(-2.0, 2.0, 6.0),
                Vec3::new(0.0, 0.0, 0.0),
                Vec3::new(0.0, 1.0, 0.0),
            )),
            translation: Translation::new(-2.0, 2.0, 6.0),
            ..Default::default()
        })
        .with(Camera);

    let wave_probe_count = 30;
    for _ in 0..wave_probe_count {
        commands
            .spawn(PbrComponents {
                mesh: wave_marker,
                material: materials.add(Color::rgb(1.0, 0.0, 0.0).into()),
                transform: Transform::new(Mat4::identity()),
                .. Default::default()
            })
            .with(WaveProbe);
    }
}


fn camera_control_system(
    time: Res<Time>,
    keyboard_input: Res<Input<KeyCode>>,
    _camera: &Camera,
    mut translation: Mut<Translation>
) {
    let speed = 10.;
    if keyboard_input.pressed(KeyCode::W) {
        *translation.z_mut() -= speed * time.delta_seconds;
    }
    if keyboard_input.pressed(KeyCode::S) {
        *translation.z_mut() += speed * time.delta_seconds;
    }
    if keyboard_input.pressed(KeyCode::A) {
        *translation.x_mut() -= speed * time.delta_seconds;
    }
    if keyboard_input.pressed(KeyCode::D) {
        *translation.x_mut() += speed * time.delta_seconds;
    }
}

fn rowing_control_system(
    mut physics: ResMut<PhysicsWorld>,
    keyboard_input: Res<Input<KeyCode>>,
    boat_handle: &Boat,
) {
    let mut boat = physics.get_body_mut(boat_handle.0).expect("Boat physics body is missing");

    let rowing_force = 150.;

    let z_force_offset = 1.5;
    // Left oar forward
    if keyboard_input.pressed(KeyCode::R) {
        boat.apply_local_force_at_local_point(
            0,
            &na::Vector3::new(rowing_force, 0., 0.),
            &na::Point3::new(-0.81, 0.605, -z_force_offset),
            ForceType::Force,
            true
        );
    }
    // Left oar backward
    if keyboard_input.pressed(KeyCode::F) {
        boat.apply_local_force_at_local_point(
            0,
            &na::Vector3::new(-rowing_force, 0., 0.),
            &na::Point3::new(-0.81, 0.605, -z_force_offset),
            ForceType::Force,
            true
        );
    }
    if keyboard_input.pressed(KeyCode::U) {
        boat.apply_local_force_at_local_point(
            0,
            &na::Vector3::new(rowing_force, 0., 0.),
            &na::Point3::new(-0.81, 0.605, z_force_offset),
            ForceType::Force,
            true
        );
    }
    // Right oar backward
    if keyboard_input.pressed(KeyCode::J) {
        boat.apply_local_force_at_local_point(
            0,
            &na::Vector3::new(-rowing_force, 0., 0.),
            &na::Point3::new(-0.81, 0.605, z_force_offset),
            ForceType::Force,
            true
        );
    }
}

fn boat_physics_graphics_updater(
    physics: Res<PhysicsWorld>,
    mut transform: Mut<Transform>,
    boat_handle: &Boat
) {
    let boat = physics.get_body(boat_handle.0).expect("Boat physics body is missing");

    let position = boat.part(0).expect("Boat body had no parts").position();
    let rotation = position.rotation.into_inner();
    let translation = position.translation.vector;

    transform.value = Mat4::from_rotation_translation(
        Quat::from_xyzw(
            rotation.coords.x,
            rotation.coords.y,
            rotation.coords.z,
            rotation.coords.w,
        ),
        Vec3::new(translation.x, translation.y, translation.z)
    );

}

fn physics_update(
    time: Res<Time>,
    mut physics: ResMut<PhysicsWorld>,
    mut physics_timer: ResMut<PhysicsTimer>
) {
    physics_timer.0 += time.delta_seconds;

    while physics_timer.0 > physics.mechanical_world.timestep() {
        physics_timer.0 -= physics.mechanical_world.timestep();

        physics.step();
    }
}


fn float_manager(
    time: Res<Time>,
    mut physics: ResMut<PhysicsWorld>,
    mut boat_query: Query<&Boat>,
    mut probes: Query<(&DepthProbe, &Translation)>
) {
    if let Some(object) = &mut boat_query.iter().iter().next()
        .and_then(|handle| physics.get_body_mut(handle.0))
    {
        let object_transform = object.part(0)
            .unwrap()
            .position()
            .to_homogeneous();

        for (DepthProbe(surface_area), translation) in &mut probes.iter() {
            let as_na = na::Point3::new(translation.x(), translation.y(), translation.z());
            let world_pos = object_transform.transform_point(&as_na);

            let water_height = water::height_at_point(
                Vec2::new(world_pos.x, world_pos.z),
                time.seconds_since_startup as f32,
            );

            let water_density = 1000.;
            // For now, assuming the water is flat
            let force_magnitude = (-(world_pos.coords.y - water_height))
                // No negative forces
                .max(0.)
                // Scaled by the volume of displaced water
                * surface_area
                * water_density;

            if force_magnitude > 0. {
                object.apply_force_at_local_point(
                    0,
                    &na::Vector3::new(0., force_magnitude, 0.),
                    &as_na,
                    ForceType::Force,
                    true
                )
            }
        }
    };
}


fn wave_probe_system(
    time: Res<Time>,
    mut depth_probes: Query<(&DepthProbe, &Transform)>,
    mut wave_probes: Query<(&WaveProbe, &mut Translation)>
) {
    for (depth_probe, mut wave_probe) in &mut depth_probes.iter().iter().zip(&mut wave_probes.iter()) {
        let probe_point = depth_probe.1.value.transform_point3(Vec3::new(0., 0., 0.));
        // println!("running wave probe")
        let height = water::height_at_point(
            Vec2::new(probe_point.x(), probe_point.z()),
            time.seconds_since_startup as f32
        );

        *wave_probe.1 = Translation::new(probe_point.x(), height, probe_point.z())
    }
}
