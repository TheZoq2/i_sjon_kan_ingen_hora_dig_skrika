import sys

import trimesh

if len(sys.argv) != 2:
    print("No model or too many arguments specified")


trimesh.util.attach_to_log();

mesh = trimesh.load(sys.argv[1])

print(mesh.center_mass)
print(mesh.moment_inertia)
