#version 450

const float M_PI = 3.1415926535897932384626433832795;

layout(location = 0) in vec3 Vertex_Position;
layout(set = 0, binding = 0) uniform Camera {
    mat4 ViewProj;
};
layout(set = 1, binding = 0) uniform Transform {
    mat4 Model;
};

layout(set = 2, binding=0) uniform WaterProperties {
    vec4 color;
    float time;
    vec3 camera;
};
// vec4 color = vec4(1., 1., 1., 1.);
// float time = 0.;
// vec3 camera = vec3(0., 0., 0.);

layout(location=1) out vec3 Vertex_Normal;
layout(location=2) out vec4 World_Position;
layout(location=3) out vec4 Original_World_Position;

float snoise(vec2);

// https://catlikecoding.com/unity/tutorials/flow/waves/

struct WaveProperties {
    float wavelength;
    float steepness;
    vec2 direction;
};

struct WaveData {
    vec3 position;
    // vec3 normal;
    vec3 binormal;
    vec3 tangent;
};

const WaveProperties wave1 = WaveProperties(2.1, 0.4, vec2(1.0, 0.0));
const WaveProperties wave2 = WaveProperties(0.9, 0.3, vec2(1.0, 0.1));
const WaveProperties wave3 = WaveProperties(0.5, 0.2, vec2(1.0, -0.2));

// Taken off https://stackoverflow.com/questions/4200224/random-noise-functions-for-glsl
float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

// float wavelength = 1.5;
// float steepness = 0.4;
// vec2 direction = vec2(1., -0.6);
WaveData gerstner_wave(
    vec3 position,
    vec3 prev_tangent,
    vec3 prev_binormal,
    WaveProperties props
) {
    vec2 d = normalize(props.direction);

    float k = 2 * M_PI / props.wavelength;
    float c = sqrt(9.8 / k); // Wave speed
    float f = k * (dot(d, position.xz) - c * time);
    float amp_noise = (1 + snoise(position.xz / 10 + vec2(time*0.1, 0)) * 0.6);
    float a = props.steepness / k * amp_noise;

    vec3 new_pos = vec3(
        position.x + d.x * (a * cos(f)),
        position.y + a * sin(f),
        position.z + d.y * (a * cos(f))
    );

    vec3 tangent = prev_tangent + vec3(
        1 - d.x * d.x * (props.steepness * sin(f)),
        d.x * (props.steepness * cos(f)),
        -d.x * d.y * (props.steepness * sin(f))
    );
    vec3 binormal = prev_binormal + vec3(
        -d.x * d.y * (props.steepness * sin(f)),
        d.y * (props.steepness * cos(f)),
        1 - d.y * d.y * (props.steepness * sin(f))
    );
    // vec3 normal = normalize(cross(binormal, tangent));

    return WaveData(new_pos, binormal, tangent);
}

void main() {

    Original_World_Position = Model * vec4(Vertex_Position, 1.0);

    vec3 tangent = vec3(1, 0, 0);
    vec3 binormal = vec3(0, 0, 1);
    WaveData wave = gerstner_wave(Original_World_Position.xyz, tangent, binormal, wave1);
    wave = gerstner_wave(wave.position, wave.tangent, wave.binormal, wave2);
    wave = gerstner_wave(wave.position, wave.tangent, wave.binormal, wave3);
    World_Position = vec4(wave.position, 1);
    gl_Position = ViewProj * World_Position;

    Vertex_Normal = normalize(cross(wave.binormal, wave.tangent));
}


// Note: These are borrowed from
//    https://github.com/ashima/webgl-noise/blob/master/src/noise2D.glsl
//
// Description : Array and textureless GLSL 2D simplex noise function.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
// 

vec3 mod289(vec3 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec2 mod289(vec2 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec3 permute(vec3 x) {
    return mod289(((x*34.0)+1.0)*x);
}

float snoise(vec2 v) {
    const vec4 C = vec4(0.211324865405187,    // (3.0-sqrt(3.0))/6.0
                                            0.366025403784439,    // 0.5*(sqrt(3.0)-1.0)
                                         -0.577350269189626,    // -1.0 + 2.0 * C.x
                                            0.024390243902439); // 1.0 / 41.0
// First corner
    vec2 i    = floor(v + dot(v, C.yy) );
    vec2 x0 = v -     i + dot(i, C.xx);

// Other corners
    vec2 i1;
    //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
    //i1.y = 1.0 - i1.x;
    i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
    // x0 = x0 - 0.0 + 0.0 * C.xx ;
    // x1 = x0 - i1 + 1.0 * C.xx ;
    // x2 = x0 - 1.0 + 2.0 * C.xx ;
    vec4 x12 = x0.xyxy + C.xxzz;
    x12.xy -= i1;

// Permutations
    i = mod289(i); // Avoid truncation effects in permutation
    vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
		+ i.x + vec3(0.0, i1.x, 1.0 ));

    vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
    m = m*m ;
    m = m*m ;

// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

    vec3 x = 2.0 * fract(p * C.www) - 1.0;
    vec3 h = abs(x) - 0.5;
    vec3 ox = floor(x + 0.5);
    vec3 a0 = x - ox;

// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
    m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );

// Compute final noise value at P
    vec3 g;
    g.x    = a0.x    * x0.x    + h.x    * x0.y;
    g.yz = a0.yz * x12.xz + h.yz * x12.yw;
    return 130.0 * dot(m, g);
}
